function main()
  
  x = [1960;    1970;   1980;   1990;   2000;   2010;   2020];
  y = [97.02; 119.21; 147.49; 179.38; 206.26; 237.63; 270.20];
  
  coeff = splinecoeff(x, y)
  
  f = @(t) spline_func(x, y, coeff, t);
  
  interpolasi_per_tahun = [
  arrayfun(f, 1960:1:1969);
  arrayfun(f, 1970:1:1979);
  arrayfun(f, 1980:1:1989);
  arrayfun(f, 1990:1:1999);
  arrayfun(f, 2000:1:2009);
  arrayfun(f, 2010:1:2019);
  [f(2020) arrayfun((@(x)0), 2021:1:2029)];
  ]
  
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  machine_error_per_decade = abs(arrayfun(f, 1960:10:2020) .- y_)'
  x = (1960:1:2020);
  plot((1960:10:2020), y_, 'o');
  hold on
  plot(x, arrayfun(f, x), '.');
  saveas(gcf, 'fig_1.png')
  hold off

endfunction