function f = spline_func(x, y, coeff, t)
  
  n = length(x);
  f = 0;
  
  for i = 1:n
    if (t < x(i))
      if (i > 1)
        f = y(i-1) + coeff(i-1, 1) * (t - x(i-1)) + coeff(i-1, 2) * (t - x(i-1))^2 + coeff(i-1, 3) * (t - x(i-1))^3; 
      endif
      break
    elseif (t == x(i))
      if (i < n)
        f = y(i) + coeff(i, 1) * (t - x(i)) + coeff(i, 2) * (t - x(i))^2 + coeff(i, 3) * (t - x(i))^3;
      else
        f = y(i-1) + coeff(i-1, 1) * (t - x(i-1)) + coeff(i-1, 2) * (t - x(i-1))^2 + coeff(i-1, 3) * (t - x(i-1))^3; 
      endif
      break
    endif
  endfor

endfunction