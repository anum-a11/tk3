function main()
  
  base_fun = @(t) ((t-2010)/30);
  [A c y] = get_ideal_matrix();
  f = @(t) (get_polynomial(c', base_fun, t));
  interpolasi_per_tahun = [
  arrayfun(f, 1960:1:1969);
  arrayfun(f, 1970:1:1979);
  arrayfun(f, 1980:1:1989);
  arrayfun(f, 1990:1:1999);
  arrayfun(f, 2000:1:2009);
  arrayfun(f, 2010:1:2019);
  [f(2020) arrayfun((@(x)0), 2021:1:2029)];
  ]
  
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  
  machine_error_per_decade = abs(arrayfun(f, 1960:10:2020) .- y_)'
  x = (1960:1:2020);
  plot(x, arrayfun(f, x), '.');
  hold on
  plot((1960:10:2020), y_, 'o');
  saveas(gcf, 'fig_1.png')
  hold off
  
  f_2030 = f(2030);
  fprintf("Ekstrapolasi tahun 2030: ")
  f_2030
  fprintf("\nDengan error: ")
  disp(abs(f_2030 - 295))
  
  interpolasi_per_tahun(end,:) = arrayfun(f, 2020:2029);
  interpolasi_per_tahun(end+1,:) = [f(2030) arrayfun((@(x)0), 2031:1:2039)]
  
  x = [x (2021:1:2030)];
  plot(x, arrayfun(f, x), '.');
  hold on
  plot((1960:10:2030), [y_, 295], 'o');
  saveas(gcf, 'fig_2.png')
  hold off
endfunction

function f = get_polynomial(c_, phi, t)
  len = size(c_, 2);
  if len == 2
    f = (c_(len-1) + (phi(t) * c_(len)));
  else
    f = c_(1) + (phi(t) * get_polynomial(c_(2:end), phi, t));
  endif
endfunction

function [A c y] = get_ideal_matrix(f)
  t_ = [1960,  1970,   1980,   1990,   2000,   2010,   2020];
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  f = @ (t, i) (((t-2010)/30)^i);
  A = gen_matrix(f, t_);
  c = A \ y_';
  y = y_;
endfunction  

function A = gen_matrix(f, t_)
  A = [];
  data_size = size(t_, 2);
  for i=1:data_size
    A(i,:) = gen_vector(f, t_(i), data_size);
  endfor
endfunction

## f = @ (t, i)
## f_1 = @ (t, i) (t^i) 
function v = gen_vector(f, t, base_len)
  v = [];
  for i =1:base_len
    v(1,i) = f(t, i-1);
  endfor
endfunction