function main()
  
  t_ = [1960,  1970,   1980,   1990,   2000,   2010,   2020];
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  
  f_A = @ (t, i) (t^i);
  f_B = @ (t, i) ((t-1970)^i);
  f_C = @ (t, i) ((t-2010)^i);
  f_D = @ (t, i) ((((t - 2010))/30)^i);
  f_E = @ (t, i) ((((t - mean(t_)))/std(t_))^i);
  norm_1 = 1;
  norm_2 = 2;
  norm_inf = Inf;
  
  norm_v = [norm_1, norm_2, norm_inf];
  proc(f_A, t_, y_, norm_v, "f_A")
  proc(f_B, t_, y_, norm_v, "f_B")
  proc(f_C, t_, y_, norm_v, "f_C")
  proc(f_D, t_, y_, norm_v, "f_D")
  proc(f_E, t_, y_, norm_v, "f_E")
  
  
endfunction

function proc(f, t_, y_, norm_v, function_name)
  fprintf("======================== %s ========================\n", function_name)
  f
  A_f = gen_matrix(f, t_)
  cond_A_f = cond(A_f)
  norm_one = norm(A_f, norm_v(1))
  norm_two = norm(A_f, norm_v(2))
  norm_infinity = norm(A_f, norm_v(3))
  c = A_f \ y_'
  err = abs((A_f * c) .- y_')
  err_avg = sum(err) / size(err, 1)
endfunction