function out()
  
  if exist('output.log', 'file') == 2
    delete('output.log');
  endif
  
  cleanupObj = onCleanup(@report);
##  fmt = format
##  format short
  diary output.log
  main()
  
  function report()
    diary off
##    format(fmt)
  endfunction
endfunction