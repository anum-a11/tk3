function A = gen_matrix(f, t_)
  A = [];
  data_size = size(t_, 2);
  for i=1:data_size
    A(i,:) = gen_vector(f, t_(i), data_size);
  endfor
endfunction

## f = @ (t, i)
## f_1 = @ (t, i) (t^i) 
function v = gen_vector(f, t, base_len)
  v = [];
  for i =1:base_len
    v(1,i) = f(t, i-1);
  endfor
endfunction