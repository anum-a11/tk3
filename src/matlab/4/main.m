function main()
  
  x = [1960;    1970;   1980;   1990;   2000;   2010;   2020];
  y = [97.02; 119.21; 147.49; 179.38; 206.26; 237.63; 270.20];
  
  Pzero = y(1);
  coeff = divided_diff(x, y)
  func_base = @(t) bases(x, t);
  P_6 = @(t) newton_div_diff(func_base(t), coeff, Pzero, 6, t);
  
  interpolasi_per_tahun = [
  arrayfun(P_6, 1960:1:1969);
  arrayfun(P_6, 1970:1:1979);
  arrayfun(P_6, 1980:1:1989);
  arrayfun(P_6, 1990:1:1999);
  arrayfun(P_6, 2000:1:2009);
  arrayfun(P_6, 2010:1:2019);
  [P_6(2020) arrayfun((@(x)0), 2021:1:2029)];
  ]
  
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  machine_error_per_decade = abs(arrayfun(P_6, 1960:10:2020) .- y_)'
  x = (1960:1:2020);
  plot((1960:10:2020), y_, 'o');
  hold on
  plot(x, arrayfun(P_6, x), '.');
  saveas(gcf, 'fig_1.png')
  hold off
  
  x = [1960;    1970;   1980;   1990;   2000;   2010;   2020; 2030];
  y = [97.02; 119.21; 147.49; 179.38; 206.26; 237.63; 270.20;  295];
  coeff = [coeff' [295; 0; 0; 0; 0; 0; 0]]'
  coeff = [coeff zeros(8,1)]
  for i = 2:8
    % M(j, i) = (M(j, i-1) - M(j-1, i-1))/ (x(j) - x(j - i + 1));
    % But only this row
    coeff(8, i) = (coeff(8, i-1) - coeff(8-1, i-1))/ (x(8) - x(8 - i + 1));
  endfor
  
  coeff

  seventh_base = @(t) func_base(t)(7);
  P_7 = @(t) P_6(t) + coeff(8, 8) * seventh_base(t);
  
  f_2030 = P_7(2030);
  fprintf("Ekstrapolasi tahun 2030: ")
  f_2030
  fprintf("\nDengan error: ")
  disp(abs(f_2030 - 295))
  
  interpolasi_per_tahun = [
  arrayfun(P_7, 1960:1:1969);
  arrayfun(P_7, 1970:1:1979);
  arrayfun(P_7, 1980:1:1989);
  arrayfun(P_7, 1990:1:1999);
  arrayfun(P_7, 2000:1:2009);
  arrayfun(P_7, 2010:1:2019);
  arrayfun(P_7, 2020:1:2029);
  [P_7(2030) arrayfun((@(x)0), 2031:1:2039)];
  ]
  
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20, 295];
  machine_error_per_decade = abs(arrayfun(P_7, 1960:10:2030) .- y_)'
  x = (1960:1:2030);
  plot((1960:10:2030), y_, 'o');
  hold on
  plot(x, arrayfun(P_7, x), '.');
  plot(x, arrayfun(P_6, x), '.');
  legend({"Data Point", "P_7", "P_6"},"location", "northwest");
  legend show;
  saveas(gcf, 'fig_2.png')
  hold off
  
endfunction