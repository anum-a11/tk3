function M = divided_diff(x, y)
  % Needs N x 1 vector as x and N x 1 vector as f(x)
  % Returns a N x N matrix 
  % whose diagonals were coefficient of the basis
  % for newton's divided differences methods
  
  [n m] = size(y);
  M = eye(n);
  M(:, 1) = y;
  
  for i = 2:n
    for j = i:n
      M(j, i) = (M(j, i-1) - M(j-1, i-1))/ (x(j) - x(j - i + 1));
    endfor
  endfor
  
endfunction