function Pn = newton_div_diff(bases, coeff, Pzero, n, t)
  
  % main Newton's Divided Differences method to find Pn(x, y)
  % That interpolates x, y
  % with input of t
  
  % Start 
  Pn = Pzero;
  
  for i = 1:n
    Pn = Pn + coeff(i+1, i+1) * bases(i);
  endfor
  
endfunction