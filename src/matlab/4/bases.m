function B = bases(x, t)
  
  % Function to get bases for newton's divided diff method
  
  [n m] = size(x);  
  B = ones(n, m);
  
  for i = 1:n
    if i == 1
      B(i) = (t - x(i));
    else
      B(i) = B(i-1) * (t - x(i));
    endif
  endfor

endfunction