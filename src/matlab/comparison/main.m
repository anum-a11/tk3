function main()
  
  % Needs 2_3, 4, and 5 functions
  addpath('../4/'); 
  addpath('../5/');
  
  % Linear System
  base_fun = @(t) ((t-2010)/30);
  [A c y] = get_ideal_matrix();
  f_polynomial = @(t) (get_polynomial(c', base_fun, t));
  
  x = [1960;    1970;   1980;   1990;   2000;   2010;   2020];
  y = [97.02; 119.21; 147.49; 179.38; 206.26; 237.63; 270.20];
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  
  % Newton's divided difference method
  Pzero = y(1);
  coeff_newton = divided_diff(x, y);
  func_base = @(t) bases(x, t);
  P_6 = @(t) newton_div_diff(func_base(t), coeff_newton, Pzero, 6, t);
  
  % Spline Method
  coeff_spline = splinecoeff(x, y);
  f_spline = @(t) spline_func(x, y, coeff_spline, t);
  
  x = (1960:1:2020);
  plot((1960:10:2020), y_, 'o');
  hold on
  plot(x, arrayfun(f_polynomial, x), '.');
  plot(x, arrayfun(P_6, x), '.');
  plot(x, arrayfun(f_spline, x), '.');
  legend({"Data Point", "f_{polynomial}", "f_{newton}", "f_{spline}"},"location", "northwest");
  legend show;
  saveas(gcf, 'fig_1.png')
  hold off

endfunction

function f = get_polynomial(c_, phi, t)
  len = size(c_, 2);
  if len == 2
    f = (c_(len-1) + (phi(t) * c_(len)));
  else
    f = c_(1) + (phi(t) * get_polynomial(c_(2:end), phi, t));
  endif
endfunction

function [A c y] = get_ideal_matrix(f)
  t_ = [1960,  1970,   1980,   1990,   2000,   2010,   2020];
  y_ = [97.02, 119.21, 147.49, 179.38, 206.26, 237.63, 270.20];
  f = @ (t, i) (((t-2010)/30)^i);
  A = gen_matrix(f, t_);
  c = A \ y_';
  y = y_;
endfunction  

function A = gen_matrix(f, t_)
  A = [];
  data_size = size(t_, 2);
  for i=1:data_size
    A(i,:) = gen_vector(f, t_(i), data_size);
  endfor
endfunction

## f = @ (t, i)
## f_1 = @ (t, i) (t^i) 
function v = gen_vector(f, t, base_len)
  v = [];
  for i =1:base_len
    v(1,i) = f(t, i-1);
  endfor
endfunction