%% adapted from Numerical analysis / Timothy Sauer,
%% George Mason University. p. 278
%% 3rd Edition, Pearson, 2019

function R = romberg_integrate(f, a, b, n)
  h = ((b-a)./(2.^(0:n-1)));
  R(1,1) = h(2)*(f(a)+f(b));
  for j = 2:n
    sum = 0;
    for k = 1:(2^(j-2))
      sum += f(a + (h(j) * ((2*k)-1)));
    endfor
    R(j, 1) = (R(j-1, 1)/2) + (h(j) * sum);
    for m = 2:j
      R(j, m) = R(j, m-1) + (R(j, m-1) - R(j-1, m-1))/((4^m) -1);
    endfor
  endfor
  
endfunction