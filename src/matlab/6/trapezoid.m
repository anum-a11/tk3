% src: https://www.mathworks.com/matlabcentral/fileexchange/72483-trapezoidal-method-composite
function answer = trapezoid(f, a, b, n)
##  f=@(x)237.630+(x-2010)/30*110.906+((x-2010)/30)^2*39.718+((x-2010)/30)^3*-109.389+((x-2010)/30)^4*-302.653+((x-2010)/30)^5*-230.172+((x-2010)/30)^6*-55.68;
##   a=input('Enter lower limit a: '); % exmple a=1
##   b=input('Enter upper limit b: ');  % exmple b=2
##   n=input('Enter the no. of subinterval: ');  % exmple n=10
   h=(b-a)/n;
   sum=0;
  for k=1:1:n-1
    x(k)=a+k*h;
    y(k)=f(x(k));
    sum=sum+y(k);
  end
  % Formula:  (h/2)*[(y0+yn)+2*(y2+y3+..+yn-1)]
  answer=h/2*(f(a)+f(b)+2*sum);
##  fprintf('\n The value of integration is %f',answer);  
  % example The value of integration is 0.410451
endfunction