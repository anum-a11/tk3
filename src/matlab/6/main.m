function main()
  
  a = 1960;
  b = 2020;
  n_romberg = [4, 8, 12, 16];
  n_simpson = [60, 120, 240, 480, 960, 1920];
  n_trapezoid = [60, 120, 240, 480, 960, 1920];
  tolerance = [2^-8, 2^-16];
  f = get_p_6();
  for n=n_romberg
    n
    romberg_integral = romberg_integrate(f, a, b, n)(end, end)
  endfor
  for n=n_simpson
    n
    composite_simpson_integral = simpson(f, a, b, n)
  endfor
  for n=n_trapezoid
    n
    composite_trapezoid_integral = trapezoid(f, a, b, n)
  endfor
  for tol=tolerance
    tol
    adaptive_quadrature = adapquad(f, a, b, tol)
  endfor
  
endfunction